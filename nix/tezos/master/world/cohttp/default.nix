/*opam-version: "2.0"
  name: "cohttp"
  version: "2.1.2"
  synopsis: "An OCaml library for HTTP clients and servers"
  description: """
  Cohttp is an OCaml library for creating HTTP daemons. It has a portable
  HTTP parser, and implementations using various asynchronous
  programming
  libraries.
  
  See the cohttp-async, cohttp-lwt, cohttp-lwt-unix, cohttp-lwt-jsoo
  and
  cohttp-mirage libraries for concrete implementations for
  particular
  targets.
  
  You can implement other targets using the parser very easily. Look at the
  `IO`
  signature in `lib/s.mli` and implement that in the desired backend.
  
  You can activate some runtime debugging by setting `COHTTP_DEBUG` to
  any
  value, and all requests and responses will be written to stderr. 
  Further
  debugging of the connection layer can be obtained by setting
  `CONDUIT_DEBUG`
  to any value."""
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy"
    "Stefano Zacchiroli"
    "David Sheets"
    "Thomas Gazagnaire"
    "David Scott"
    "Rudi Grinberg"
    "Andy Ray"
  ]
  license: "ISC"
  tags: ["org:mirage" "org:xapi-project"]
  homepage: "https://github.com/mirage/ocaml-cohttp"
  doc: "https://mirage.github.io/ocaml-cohttp/"
  bug-reports: "https://github.com/mirage/ocaml-cohttp/issues"
  depends: [
    "ocaml" {>= "4.04.1"}
    "dune" {build & >= "1.1.0"}
    "re" {>= "1.7.2"}
    "uri" {>= "2.0.0"}
    "fieldslib"
    "sexplib0"
    "ppx_fields_conv" {>= "v0.9.0"}
    "ppx_sexp_conv" {>= "v0.9.0"}
    "stringext"
    "base64" {>= "3.1.0"}
    "fmt" {with-test}
    "jsonm" {build}
    "alcotest" {with-test}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-cohttp.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-cohttp/releases/download/v2.1.2/cohttp-v2.1.2.tbz"
    checksum: [
      "md5=1bdde705d853175c67666b45bf3edc43"
     
  "sha256=ea68192caf938f3bb68811ad1b424c080d2c4291a9e5b166a2772d91bb88a865"
     
  "sha512=29c64a41b4c28e3a69e83f03632ace0f1c4a75add8ec401ed7eb2a4fc7074948e62529174056242cf84bdc25ba1365f66176538c1b7e4d0896a626f3bdb5aeea"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, re, uri, fieldslib, sexplib0, ppx_fields_conv,
  ppx_sexp_conv, stringext, base64, fmt ? null, jsonm, alcotest ? null,
  findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "2.1.2"; in
assert (vcompare ocaml "4.04.1") >= 0;
assert (vcompare dune "1.1.0") >= 0;
assert (vcompare re "1.7.2") >= 0;
assert (vcompare uri "2.0.0") >= 0;
assert (vcompare ppx_fields_conv "v0.9.0") >= 0;
assert (vcompare ppx_sexp_conv "v0.9.0") >= 0;
assert (vcompare base64 "3.1.0") >= 0;

stdenv.mkDerivation rec {
  pname = "cohttp";
  version = "2.1.2";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-cohttp/releases/download/v2.1.2/cohttp-v2.1.2.tbz";
    sha256 = "0rd8i2xr2bbpl9kb3rd9j512q3889i11pb8ii2v3p3wkmwn1js7a";
  };
  buildInputs = [
    ocaml dune re uri fieldslib sexplib0 ppx_fields_conv ppx_sexp_conv
    stringext base64 ]
  ++
  stdenv.lib.optional
  doCheck
  fmt
  ++
  [
    jsonm ]
  ++
  stdenv.lib.optional
  doCheck
  alcotest
  ++
  [
    findlib ];
  propagatedBuildInputs = [
    ocaml dune re uri fieldslib sexplib0 ppx_fields_conv ppx_sexp_conv
    stringext base64 ]
  ++
  stdenv.lib.optional
  doCheck
  fmt
  ++
  stdenv.lib.optional
  doCheck
  alcotest;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
