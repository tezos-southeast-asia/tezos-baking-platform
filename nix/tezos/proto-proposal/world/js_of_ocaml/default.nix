/*opam-version: "2.0"
  name: "js_of_ocaml"
  version: "3.3.0"
  synopsis: "Compiler from OCaml bytecode to Javascript"
  maintainer: "dev@ocsigen.org"
  authors: "Ocsigen team"
  homepage: "http://ocsigen.org/js_of_ocaml"
  bug-reports: "https://github.com/ocsigen/js_of_ocaml/issues"
  depends: [
    "ocaml" {>= "4.02.0" & < "4.08.0"}
    "dune" {build & >= "1.2"}
    "ocaml-migrate-parsetree"
    "ppx_tools_versioned"
    "uchar"
    "js_of_ocaml-compiler"
  ]
  conflicts: [
    "ppx_tools_versioned" {<= "5.0beta0"}
  ]
  build: ["dune" "build" "-p" name "-j" jobs]
  dev-repo: "git+https://github.com/ocsigen/js_of_ocaml.git"
  url {
    src: "https://github.com/ocsigen/js_of_ocaml/archive/3.3.0.tar.gz"
    checksum: [
      "md5=438051f64e307edbda42f250a3d9cef1"
     
  "sha256=6cd367d1b7338374c2d3b6fbb5bfc671cfd4bb718580bd7b279e4c4cd2ee6dc4"
     
  "sha512=4869b3f337119127dbf71feafa5ecf7a507e0c19c5e59d49793b718207556ccf0a796c4de4526bfd3c982240f6b2dda70d299d27b0300223aca42067deb612a8"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, ocaml-migrate-parsetree, ppx_tools_versioned, uchar,
  js_of_ocaml-compiler, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "3.3.0"; in
assert (vcompare ocaml "4.02.0") >= 0 && (vcompare ocaml "4.08.0") < 0;
assert (vcompare dune "1.2") >= 0;
assert !((vcompare ppx_tools_versioned "5.0beta0") <= 0);

stdenv.mkDerivation rec {
  pname = "js_of_ocaml";
  version = "3.3.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocsigen/js_of_ocaml/archive/3.3.0.tar.gz";
    sha256 = "1i3dxv94qk4y4xxvv045f6xx9kviqszvbyxnsg1790rknz8nglvc";
  };
  buildInputs = [
    ocaml dune ocaml-migrate-parsetree ppx_tools_versioned uchar
    js_of_ocaml-compiler findlib ];
  propagatedBuildInputs = [
    ocaml dune ocaml-migrate-parsetree ppx_tools_versioned uchar
    js_of_ocaml-compiler ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
