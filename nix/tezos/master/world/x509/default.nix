/*opam-version: "2.0"
  name: "x509"
  version: "0.6.3"
  synopsis: "Public Key Infrastructure (RFC 5280, PKCS) purely in
  OCaml"
  description: """
  X.509 is a public key infrastructure used mostly on the Internet.  It
  consists
  of certificates which include public keys and identifiers, signed by
  an
  authority. Authorities must be exchanged over a second channel to establish
  the
  trust relationship. This library implements most parts of RFC5280 and
  RFC6125.
  The Public Key Cryptography Standards (PKCS) defines encoding and
  decoding,
  which is also partially implemented by this library - namely PKCS 1, PKCS
  7,
  PKCS 8, PKCS 9 and PKCS 10."""
  maintainer: [
    "Hannes Mehnert <hannes@mehnert.org>" "David Kaloper
  <david@numm.org>"
  ]
  authors: [
    "David Kaloper <david@numm.org>" "Hannes Mehnert
  <hannes@mehnert.org>"
  ]
  license: "BSD2"
  tags: "org:mirage"
  homepage: "https://github.com/mirleft/ocaml-x509"
  doc: "https://mirleft.github.io/ocaml-x509/doc"
  bug-reports: "https://github.com/mirleft/ocaml-x509/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "dune" {build & >= "1.2"}
    "ppx_sexp_conv" {>= "v0.11.0"}
    "cstruct" {>= "4.0.0"}
    "cstruct-sexp"
    "sexplib"
    "asn1-combinators" {>= "0.2.0"}
    "ptime"
    "nocrypto" {>= "0.5.3"}
    "astring"
    "ounit" {with-test}
    "cstruct-unix" {with-test & >= "3.0.0"}
  ]
  conflicts: [
    "ppx_sexp_conv" {= "v0.11.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirleft/ocaml-x509.git"
  url {
    src:
     
  "https://github.com/mirleft/ocaml-x509/releases/download/0.6.3/x509-0.6.3.tbz"
    checksum: [
      "md5=52fbcf55fd7fe775c0b33a81f04a92cf"
     
  "sha256=adfeb9d58f99b92e5a57f2542c72127d161f6473072d86763c8daf383184c340"
     
  "sha512=867e79cc8cd9f56be6f1a68bd593048c66ce4d7ac09933c9daab19e7dcb2cada543e41b50713b791284f7cfa85b7f46d1a92a89510c880633750060b07030fdd"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, ppx_sexp_conv, cstruct, cstruct-sexp, sexplib,
  asn1-combinators, ptime, nocrypto, astring, ounit ? null,
  cstruct-unix ? null, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.6.3"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare dune "1.2") >= 0;
assert (vcompare ppx_sexp_conv "v0.11.0") >= 0;
assert (vcompare cstruct "4.0.0") >= 0;
assert (vcompare asn1-combinators "0.2.0") >= 0;
assert (vcompare nocrypto "0.5.3") >= 0;
assert doCheck -> (vcompare cstruct-unix "3.0.0") >= 0;
assert stdenv.lib.getVersion ppx_sexp_conv != "v0.11.0";

stdenv.mkDerivation rec {
  pname = "x509";
  version = "0.6.3";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirleft/ocaml-x509/releases/download/0.6.3/x509-0.6.3.tbz";
    sha256 = "0h63hhqkibwd7iv8cb87fdj1y5kx29r2qm7jaxd2xfcrizavkzmd";
  };
  buildInputs = [
    ocaml dune ppx_sexp_conv cstruct cstruct-sexp sexplib asn1-combinators
    ptime nocrypto astring ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    cstruct-unix findlib ];
  propagatedBuildInputs = [
    ocaml dune ppx_sexp_conv cstruct cstruct-sexp sexplib asn1-combinators
    ptime nocrypto astring ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  [
    cstruct-unix ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
