/*opam-version: "2.0"
  name: "conf-libsodium"
  version: "1"
  synopsis: "Virtual package relying on a libsodium system
  installation"
  description:
    "This package can only install if the libsodium is installed on the
  system."
  maintainer: "Grégoire Henry <gregoire.henry@ocamlpro.com>"
  authors: [
    "Adam Langley"
    "Alex Biryukov"
    "Bo-Yin Yang"
    "Christian Winnerlein"
    "Colin Percival"
    "Daniel Dinu"
    "Daniel J. Bernstein"
    "Dmitry Khovratovich"
    "Jean-Philippe Aumasson"
    "Niels Duif"
    "Peter Schwabe"
    "Samuel Neves"
    "Tanja Lange"
    "Zooko Wilcox-O'Hearn"
  ]
  license: "ISC"
  homepage: "https://download.libsodium.org/doc/"
  bug-reports: "https://github.com/ocaml/opam-repository/issues"
  depends: [
    "conf-pkg-config" {build}
  ]
  flags: conf
  build: ["pkg-config" "libsodium"]
  depexts: [
    ["libsodium-dev"] {os-distribution = "debian"}
    ["libsodium-dev"] {os-distribution = "ubuntu"}
    ["security/libsodium"] {os = "freebsd"}
    ["libsodium"] {os-distribution = "homebrew" & os = "macos"}
    ["libsodium-dev"] {os-distribution = "alpine"}
  ]*/
{ runCommand, doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl,
  conf-pkg-config, findlib, libsodium }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in

stdenv.mkDerivation rec {
  pname = "conf-libsodium";
  version = "1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = runCommand
  "empty"
  {
    outputHashMode = "recursive";
    outputHashAlgo = "sha256";
    outputHash = "0sjjj9z1dhilhpc8pq4154czrb79z9cm044jvn75kxcjv6v5l2m5";
  }
  "mkdir $out";
  buildInputs = [
    conf-pkg-config findlib libsodium ];
  propagatedBuildInputs = [
    libsodium ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'pkg-config'" "'libsodium'" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
