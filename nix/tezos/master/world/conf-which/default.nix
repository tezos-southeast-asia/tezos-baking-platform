/*opam-version: "2.0"
  name: "conf-which"
  version: "1"
  synopsis: "Virtual package relying on which"
  description:
    "This package can only install if the which program is installed on the
  system."
  maintainer: "unixjunkie@sdf.org"
  authors: "Carlo Wood"
  license: "GPL-2+"
  homepage: "http://www.gnu.org/software/which/"
  bug-reports: "https://github.com/ocaml/opam-repository/issues"
  flags: conf
  build: ["which" "which"]
  depexts: [
    ["which"] {os-distribution = "centos"}
    ["which"] {os-distribution = "fedora"}
    ["which"] {os-family = "suse"}
    ["debianutils"] {os-family = "debian"}
    ["which"] {os-distribution = "nixos"}
    ["which"] {os-distribution = "arch"}
  ]*/
{ runCommand, doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv,
  opam, fetchurl, findlib, which }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1"; in

stdenv.mkDerivation rec {
  pname = "conf-which";
  version = "1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = runCommand
  "empty"
  {
    outputHashMode = "recursive";
    outputHashAlgo = "sha256";
    outputHash = "0sjjj9z1dhilhpc8pq4154czrb79z9cm044jvn75kxcjv6v5l2m5";
  }
  "mkdir $out";
  buildInputs = [
    findlib which ];
  propagatedBuildInputs = [
    which ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'which'" "'which'" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
