/*opam-version: "2.0"
  name: "ctypes"
  version: "0.14.0"
  synopsis: "Combinators for binding to C libraries without writing any
  C."
  description: """
  ctypes is a library for binding to C libraries using pure OCaml. The
  primary
  aim is to make writing C extensions as straightforward as possible.
  
  The core of ctypes is a set of combinators for describing the structure of
  C
  types -- numeric types, arrays, pointers, structs, unions and functions.
  You
  can use these combinators to describe the types of the functions that you
  want
  to call, then bind directly to those functions -- all without writing
  or
  generating any C!
  
  To install the optional `ctypes.foreign` interface (which uses `libffi`
  to
  provide dynamic access to foreign libraries), you will need to also
  install
  the `ctypes-foreign` optional dependency:
  
      opam install ctypes ctypes-foreign
  
  This will make the `ctypes.foreign` ocamlfind subpackage
  available."""
  maintainer: "yallop@gmail.com"
  authors: "yallop@gmail.com"
  license: "MIT"
  tags: ["org:ocamllabs" "org:mirage"]
  homepage: "https://github.com/ocamllabs/ocaml-ctypes"
  doc: "http://ocamllabs.github.io/ocaml-ctypes"
  bug-reports: "http://github.com/ocamllabs/ocaml-ctypes/issues"
  depends: [
    "ocaml" {>= "4.01.0"}
    "base-bytes"
    "integers" {< "0.3.0"}
    "ocamlfind" {build}
    "conf-pkg-config" {build}
    "lwt" {with-test & >= "2.4.7" & < "4.0.0"}
    "ounit" {with-test}
    "ctypes-foreign" {with-test}
    "conf-ncurses" {with-test}
  ]
  depopts: ["ctypes-foreign" "mirage-xen"]
  flags: light-uninstall
  build: [
    [make "XEN=%{mirage-xen:enable}%" "libffi.config"]
      {ctypes-foreign:installed}
    ["touch" "libffi.config"] {!ctypes-foreign:installed}
    [make "XEN=%{mirage-xen:enable}%" "ctypes-base" "ctypes-stubs"]
    [make "XEN=%{mirage-xen:enable}%" "ctypes-foreign"]
      {ctypes-foreign:installed}
    [make "test"] {with-test}
  ]
  install: [make "install" "XEN=%{mirage-xen:enable}%"]
  remove: ["ocamlfind" "remove" "ctypes"]
  dev-repo: "git+http://github.com/ocamllabs/ocaml-ctypes.git"
  url {
    src: "https://github.com/ocamllabs/ocaml-ctypes/archive/0.14.0.tar.gz"
    checksum: "md5=57875f5e5986ca6004c356d77a3b31b5"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml,
  base-bytes, integers, findlib, conf-pkg-config, lwt ? null, ounit ? null,
  ctypes-foreign ? null, conf-ncurses ? null, mirage-xen ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.01.0") >= 0;
assert (vcompare integers "0.3.0") < 0;
assert doCheck -> (vcompare lwt "2.4.7") >= 0 && (vcompare lwt "4.0.0") < 0;

stdenv.mkDerivation rec {
  pname = "ctypes";
  version = "0.14.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocamllabs/ocaml-ctypes/archive/0.14.0.tar.gz";
    sha256 = "0zrsd42q2nciyg9375g2kydqax6ay299rhyfgms59qiw7d9ylyp9";
  };
  buildInputs = [
    ocaml base-bytes integers findlib conf-pkg-config lwt ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  stdenv.lib.optional
  (doCheck
  ||
  ctypes-foreign
  !=
  null)
  ctypes-foreign
  ++
  stdenv.lib.optional
  doCheck
  conf-ncurses
  ++
  stdenv.lib.optional
  (mirage-xen
  !=
  null)
  mirage-xen;
  propagatedBuildInputs = [
    ocaml base-bytes integers lwt ]
  ++
  stdenv.lib.optional
  doCheck
  ounit
  ++
  stdenv.lib.optional
  (doCheck
  ||
  ctypes-foreign
  !=
  null)
  ctypes-foreign
  ++
  stdenv.lib.optional
  doCheck
  conf-ncurses
  ++
  stdenv.lib.optional
  (mirage-xen
  !=
  null)
  mirage-xen;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    (stdenv.lib.optionals (ctypes-foreign != null) [
      "make"
      "'XEN='${if mirage-xen != null then "enable" else "disable"}" "'libffi.config'" ])
      (stdenv.lib.optionals (ctypes-foreign == null) [
        "'touch'" "'libffi.config'" ])
      [
        "make"
        "'XEN='${if mirage-xen != null then "enable" else "disable"}" "'ctypes-base'" "'ctypes-stubs'" ]
        (stdenv.lib.optionals (ctypes-foreign != null) [
          "make"
          "'XEN='${if mirage-xen != null then "enable" else "disable"}" "'ctypes-foreign'" ])
          (stdenv.lib.optionals doCheck [ "make" "'test'" ]) ];
        preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
        [
          [
            "make" "'install'"
            "'XEN='${if mirage-xen != null then "enable" else "disable"}" ] ];
          installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
          createFindlibDestdir = true;
        }
