/*opam-version: "2.0"
  name: "mtime"
  version: "1.2.0"
  synopsis: "Monotonic wall-clock time for OCaml"
  description: """
  Mtime has platform independent support for monotonic wall-clock time
  in pure OCaml. This time increases monotonically and is not subject
  to
  operating system calendar time adjustments. The library has types
  to
  represent nanosecond precision timestamps and time spans.
  
  The additional Mtime_clock library provide access to a system
  monotonic clock.
  
  Mtime has a no dependency. Mtime_clock depends on your system library.
  The optional JavaScript support depends on [js_of_ocaml][jsoo]. Mtime
  and its libraries are distributed under the ISC license.
  
  [jsoo]: http://ocsigen.org/js_of_ocaml/"""
  maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  authors: "The mtime programmers"
  license: "ISC"
  tags: ["time" "monotonic" "system" "org:erratique"]
  homepage: "https://erratique.ch/software/mtime"
  doc: "https://erratique.ch/software/mtime"
  bug-reports: "https://github.com/dbuenzli/mtime/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
    "topkg" {build}
  ]
  depopts: ["js_of_ocaml"]
  conflicts: [
    "js_of_ocaml" {<= "3.3.0"}
  ]
  build: [
    "ocaml"
    "pkg/pkg.ml"
    "build"
    "--pinned"
    "%{pinned}%"
    "--with-js_of_ocaml"
    "%{js_of_ocaml:installed}%"
  ]
  dev-repo: "git+https://erratique.ch/repos/mtime.git"
  url {
    src: "https://erratique.ch/software/mtime/releases/mtime-1.2.0.tbz"
    checksum: [
      "md5=f3f4c1333c0f74fc27b05c35b9c0dab9"
     
  "sha256=687564bcddf22b096f9ac7b5f7f24bd7c62f70da6b8daa9dfdf08d3ff196a17e"
     
  "sha512=0f0ed220cd0f899643930814010f8592e1f47b6dc6c4dce8eebfb17a81b0abed093a3dbf9c02490af99bf81ea640372f73b4ab88fd8ef0e0c7c66920692a8778"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild, topkg, js_of_ocaml ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.2.0"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert js_of_ocaml != null -> !((vcompare js_of_ocaml "3.3.0") <= 0);

stdenv.mkDerivation rec {
  pname = "mtime";
  version = "1.2.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://erratique.ch/software/mtime/releases/mtime-1.2.0.tbz";
    sha256 = "0zm1jvqkz3ghznfsm3bbv9q2zinp9grggdf7k9phjazjvny68xb8";
  };
  buildInputs = [
    ocaml findlib ocamlbuild topkg ]
  ++
  stdenv.lib.optional
  (js_of_ocaml
  !=
  null)
  js_of_ocaml;
  propagatedBuildInputs = [
    ocaml ]
  ++
  stdenv.lib.optional
  (js_of_ocaml
  !=
  null)
  js_of_ocaml;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--pinned'" "false"
      "'--with-js_of_ocaml'"
      "${if js_of_ocaml != null then "true" else "false"}" ] ];
    preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
    [ ];
    installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
    createFindlibDestdir = true;
  }
