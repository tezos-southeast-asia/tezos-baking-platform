/*opam-version: "2.0"
  name: "cppo"
  version: "1.6.5"
  synopsis: "Equivalent of the C preprocessor for OCaml programs"
  maintainer: "martin@mjambon.com"
  authors: "Martin Jambon"
  license: "BSD-3-Clause"
  homepage: "https://github.com/mjambon/cppo"
  bug-reports: "https://github.com/mjambon/cppo/issues"
  depends: [
    "ocaml"
    "jbuilder" {build & >= "1.0+beta17"}
    "base-unix"
  ]
  build: [
    ["jbuilder" "subst" "-p" name] {pinned}
    ["jbuilder" "build" "-p" name "-j" jobs]
    ["jbuilder" "runtest" "-p" name] {with-test}
  ]
  dev-repo: "git+https://github.com/mjambon/cppo.git"
  url {
    src: "https://github.com/mjambon/cppo/archive/v1.6.5.tar.gz"
    checksum: "md5=1cd25741d31417995b0973fe0b6f6c82"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml,
  jbuilder, base-unix, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare jbuilder "1.0+beta17") >= 0;

stdenv.mkDerivation rec {
  pname = "cppo";
  version = "1.6.5";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mjambon/cppo/archive/v1.6.5.tar.gz";
    sha256 = "1dkm3d5h6h56y937gcdk2wixlpzl59vv5pmiafglr89p20kf7gqf";
  };
  buildInputs = [
    ocaml jbuilder base-unix findlib ];
  propagatedBuildInputs = [
    ocaml jbuilder base-unix ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'jbuilder'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'jbuilder'" "'runtest'" "'-p'" pname ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
