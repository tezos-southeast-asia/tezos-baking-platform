{ lib, stdenv, tezos-src, alcotest, alcotest-lwt, base, base-unix, bigstring, cmdliner, cohttp-lwt, cohttp-lwt-unix, conf-gmp, conf-libev, cstruct, dum, dune, ezjsonm, genspio, hex, ocaml-hidapi, ipaddr, irmin, js_of_ocaml, lwt, lwt_log, macaddr, mtime, ocaml, findlib, ocamlformat, ocp-ocamlres, ocplib-endian, ptime, re, rresult, stdio, tls, uri, uutf, zarith }:

assert lib.versionAtLeast (lib.getVersion bigstring) "0.1.1";
assert lib.versionAtLeast (lib.getVersion cstruct) "3.2.1";
assert lib.versionAtLeast (lib.getVersion ocplib-endian) "1.0";
assert lib.versionAtLeast (lib.getVersion re) "1.7.2";
assert lib.versionAtLeast (lib.getVersion zarith) "1.7";

let ifPathExists = p: c: if lib.isStorePath (builtins.dirOf p) then "[ -e ${p} ] && ${c}" else lib.optionalString (builtins.pathExists p) c; in
stdenv.mkDerivation rec {
  name = "tezos-${version}";
  version = "0.0.0";
  srcs = builtins.map (p: tezos-src + p) [ /src /vendors /scripts /docs ];
  postUnpack = lib.concatMapStrings
    (p: ifPathExists (tezos-src + p)
      "cp ${tezos-src + p} ${lib.escapeShellArg (builtins.baseNameOf p)}\n")
    [ /active_protocol_versions /dune /dune-workspace
      /jbuild /Makefile ];
  sourceRoot = ".";
  buildInputs = [ alcotest alcotest-lwt base base-unix bigstring cmdliner cohttp-lwt cohttp-lwt-unix conf-gmp conf-libev cstruct dum dune ezjsonm genspio hex ocaml-hidapi ipaddr irmin js_of_ocaml lwt lwt_log macaddr mtime ocaml findlib ocamlformat ocp-ocamlres ocplib-endian ptime re rresult stdio tls uri uutf zarith ];

  makeFlags = [ "current_opam_version=2.0.0" "current_ocaml_version=${ocaml.version}" ];

  buildFlags = [ "all" "build-sandbox" ];

  postInstall = ''cp tezos-sandbox "$out/bin"'';

  meta = {
    homepage = https://tezos.com;
    description = "Tezos blockchain reference implementation.";
    license = stdenv.lib.licenses.mit;
    inherit (ocaml.meta) platforms;
  };
}
