/*opam-version: "2.0"
  name: "cmdliner"
  version: "1.0.3"
  synopsis: "Declarative definition of command line interfaces for
  OCaml"
  description: """
  Cmdliner allows the declarative definition of command line interfaces
  for OCaml.
  
  It provides a simple and compositional mechanism to convert command
  line arguments to OCaml values and pass them to your functions. The
  module automatically handles syntax errors, help messages and UNIX man
  page generation. It supports programs with single or multiple commands
  and respects most of the [POSIX][1] and [GNU][2] conventions.
  
  Cmdliner has no dependencies and is distributed under the ISC license.
  
  [1]:
  http://pubs.opengroup.org/onlinepubs/009695399/basedefs/xbd_chap12.html
  [2]:
  http://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html"""
  maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  authors: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  license: "ISC"
  tags: ["cli" "system" "declarative" "org:erratique"]
  homepage: "http://erratique.ch/software/cmdliner"
  doc: "http://erratique.ch/software/cmdliner/doc/Cmdliner"
  bug-reports: "https://github.com/dbuenzli/cmdliner/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
  ]
  build: [make "all" "PREFIX=%{prefix}%"]
  install: [
    [make "install" "LIBDIR=%{_:lib}%" "DOCDIR=%{_:doc}%"]
    [make "install-doc" "LIBDIR=%{_:lib}%" "DOCDIR=%{_:doc}%"]
  ]
  dev-repo: "git+http://erratique.ch/repos/cmdliner.git"
  url {
    src: "http://erratique.ch/software/cmdliner/releases/cmdliner-1.0.3.tbz"
    checksum: "md5=3674ad01d4445424105d33818c78fba8"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml, findlib
  }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.03.0") >= 0;

stdenv.mkDerivation rec {
  pname = "cmdliner";
  version = "1.0.3";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "http://erratique.ch/software/cmdliner/releases/cmdliner-1.0.3.tbz";
    sha256 = "0g3w4hvc1cx9x2yp5aqn6m2rl8lf9x1dn754hfq8m1sc1102lxna";
  };
  buildInputs = [
    ocaml findlib ];
  propagatedBuildInputs = [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" "'all'" "'PREFIX='$out" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "make" "'install'" "'LIBDIR='$OCAMLFIND_DESTDIR/${pname}"
      "'DOCDIR='$out/share/doc/${pname}" ]
    [
      "make" "'install-doc'" "'LIBDIR='$OCAMLFIND_DESTDIR/${pname}"
      "'DOCDIR='$out/share/doc/${pname}" ]
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
