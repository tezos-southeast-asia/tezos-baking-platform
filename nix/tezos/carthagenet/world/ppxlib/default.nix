/*opam-version: "2.0"
  name: "ppxlib"
  version: "0.8.1"
  synopsis: "Base library and tools for ppx rewriters"
  description: """
  A comprehensive toolbox for ppx development. It features:
  - a OCaml AST / parser / pretty-printer snapshot,to create a full
     frontend independent of the version of OCaml;
  - a library for library for ppx rewriters in general, and type-driven
    code generators in particular;
  - a feature-full driver for OCaml AST transformers;
  - a quotation mechanism allowing  to write values representing the
     OCaml AST in the OCaml syntax;
  - a generator of open recursion classes from type
  definitions."""
  maintainer: "opensource@janestreet.com"
  authors: "Jane Street Group, LLC <opensource@janestreet.com>"
  license: "MIT"
  homepage: "https://github.com/ocaml-ppx/ppxlib"
  doc: "https://ocaml-ppx.github.io/ppxlib/"
  bug-reports: "https://github.com/ocaml-ppx/ppxlib/issues"
  depends: [
    "ocaml" {>= "4.04.1"}
    "base" {>= "v0.11.0"}
    "dune"
    "ocaml-compiler-libs" {>= "v0.11.0"}
    "ocaml-migrate-parsetree" {>= "1.3.1"}
    "ppx_derivers" {>= "1.0"}
    "stdio" {>= "v0.11.0"}
    "ocamlfind" {with-test}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  run-test:
    ["dune" "runtest" "-p" name "-j" jobs]
      {ocaml:version >= "4.06" & ocaml:version < "4.08"}
  dev-repo: "git+https://github.com/ocaml-ppx/ppxlib.git"
  url {
    src:
     
  "https://github.com/ocaml-ppx/ppxlib/releases/download/0.8.1/ppxlib-0.8.1.tbz"
    checksum: [
     
  "sha256=a5cb79ee83bba80304b65bc47f2985382bef89668b1b46f9ffb3734c2f2f7521"
     
  "sha512=74bf4a0811f4fa73969149efc7f98620bf1c1ef7322edb8de82e02e25b61e005945887ea865b462bfb638d7d0e574706da190ca9416643f4464a89262ae7ae12"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base, dune, ocaml-compiler-libs, ocaml-migrate-parsetree,
  ppx_derivers, stdio, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.8.1"; in
assert (vcompare ocaml "4.04.1") >= 0;
assert (vcompare base "v0.11.0") >= 0;
assert (vcompare ocaml-compiler-libs "v0.11.0") >= 0;
assert (vcompare ocaml-migrate-parsetree "1.3.1") >= 0;
assert (vcompare ppx_derivers "1.0") >= 0;
assert (vcompare stdio "v0.11.0") >= 0;

stdenv.mkDerivation rec {
  pname = "ppxlib";
  version = "0.8.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml-ppx/ppxlib/releases/download/0.8.1/ppxlib-0.8.1.tbz";
    sha256 = "08bm5wplqwxkzzwlc6wbcs4yyarqhllpzi2vnq207a5vhgp7kjx5";
  };
  buildInputs = [
    ocaml base dune ocaml-compiler-libs ocaml-migrate-parsetree ppx_derivers
    stdio findlib ];
  propagatedBuildInputs = [
    ocaml base dune ocaml-compiler-libs ocaml-migrate-parsetree ppx_derivers
    stdio ]
  ++
  stdenv.lib.optional
  doCheck
  findlib;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
