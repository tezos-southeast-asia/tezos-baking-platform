/*opam-version: "2.0"
  name: "cstruct-lwt"
  version: "4.0.0"
  synopsis: "Access C-like structures directly from OCaml"
  description: """
  Cstruct is a library and syntax extension to make it easier to access
  C-like
  structures directly from OCaml.  It supports both reading and writing to
  these
  structures, and they are accessed via the `Bigarray` module."""
  maintainer: "anil@recoil.org"
  authors: [
    "Anil Madhavapeddy"
    "Richard Mortier"
    "Thomas Gazagnaire"
    "Pierre Chambart"
    "David Kaloper"
    "Jeremy Yallop"
    "David Scott"
    "Mindy Preston"
    "Thomas Leonard"
    "Etienne Millon"
  ]
  license: "ISC"
  tags: ["org:mirage" "org:ocamllabs"]
  homepage: "https://github.com/mirage/ocaml-cstruct"
  doc: "https://mirage.github.io/ocaml-cstruct/"
  bug-reports: "https://github.com/mirage/ocaml-cstruct/issues"
  depends: [
    "ocaml" {>= "4.03.0"}
    "base-unix"
    "lwt"
    "cstruct" {= version}
    "dune" {build & >= "1.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
  ]
  dev-repo: "git+https://github.com/mirage/ocaml-cstruct.git"
  url {
    src:
     
  "https://github.com/mirage/ocaml-cstruct/releases/download/v4.0.0/cstruct-v4.0.0.tbz"
    checksum: [
      "md5=e01a2036d1fb08d1d8e47d4f3e122bf0"
     
  "sha256=2a54e291ccf71691fd80b5b80600e2dd5e68d91b8b8f2788f326355305d38ee0"
     
  "sha512=519fbfa045eeb7d1fc791cda2d88025853e186af73308de9750162a95dafb667711eac280b38c68761d7cfe38801eb35704026f7faebc091b61c04807aaebc4c"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, base-unix, lwt, cstruct, dune, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "4.0.0"; in
assert (vcompare ocaml "4.03.0") >= 0;
assert stdenv.lib.getVersion cstruct == version;
assert (vcompare dune "1.0") >= 0;

stdenv.mkDerivation rec {
  pname = "cstruct-lwt";
  version = "4.0.0";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirage/ocaml-cstruct/releases/download/v4.0.0/cstruct-v4.0.0.tbz";
    sha256 = "1q4fsc2m6d96yf42g3wb3gcnhpnxw800df5mh3yr25pprj8y4m1a";
  };
  buildInputs = [
    ocaml base-unix lwt cstruct dune findlib ];
  propagatedBuildInputs = [
    ocaml base-unix lwt cstruct dune ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
