/*opam-version: "2.0"
  name: "fmt"
  version: "0.8.8"
  synopsis: "OCaml Format pretty-printer combinators"
  description: """
  Fmt exposes combinators to devise `Format` pretty-printing functions.
  
  Fmt depends only on the OCaml standard library. The optional
  `Fmt_tty`
  library that allows to setup formatters for terminal color output
  depends on the Unix library. The optional `Fmt_cli` library that
  provides command line support for Fmt depends on [`Cmdliner`][cmdliner].
  
  Fmt is distributed under the ISC license.
  
  [cmdliner]: http://erratique.ch/software/cmdliner"""
  maintainer: "Daniel Bünzli <daniel.buenzl i@erratique.ch>"
  authors: "The fmt programmers"
  license: "ISC"
  tags: ["string" "format" "pretty-print" "org:erratique"]
  homepage: "https://erratique.ch/software/fmt"
  doc: "https://erratique.ch/software/fmt"
  bug-reports: "https://github.com/dbuenzli/fmt/issues"
  depends: [
    "ocaml" {>= "4.05.0"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
    "topkg" {build & >= "0.9.0"}
    "seq"
    "stdlib-shims"
  ]
  depopts: ["base-unix" "cmdliner"]
  conflicts: [
    "cmdliner" {< "0.9.8"}
  ]
  build: [
    "ocaml"
    "pkg/pkg.ml"
    "build"
    "--dev-pkg"
    "%{pinned}%"
    "--with-base-unix"
    "%{base-unix:installed}%"
    "--with-cmdliner"
    "%{cmdliner:installed}%"
  ]
  dev-repo: "git+https://erratique.ch/repos/fmt.git"
  url {
    src: "https://erratique.ch/software/fmt/releases/fmt-0.8.8.tbz"
    checksum: [
      "md5=473490fcfdf3ff0a8ccee226b873d4b2"
     
  "sha256=64eeaf0659fe3b8cbf334ac6d55ff41d04310b400f895142ae028c6627cfc0c7"
     
  "sha512=be664b4b5f0a0611bf8bbe7d0b242c6efe1a360f5c536c9d01b20ee99fb0e2de4d74b62cbf36bf30213a98f37243f31b2b069d0c3b2f739c90fb6639b336c883"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, ocamlbuild, topkg, seq, stdlib-shims,
  base-unix ? null, cmdliner ? null }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.8.8"; in
assert (vcompare ocaml "4.05.0") >= 0;
assert (vcompare topkg "0.9.0") >= 0;
assert cmdliner != null -> !((vcompare cmdliner "0.9.8") < 0);

stdenv.mkDerivation rec {
  pname = "fmt";
  version = "0.8.8";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://erratique.ch/software/fmt/releases/fmt-0.8.8.tbz";
    sha256 = "1iy0rwknd302mr15328g805k210xyigxbija6fzqqfzyb43azvk4";
  };
  buildInputs = [
    ocaml findlib ocamlbuild topkg seq stdlib-shims ]
  ++
  stdenv.lib.optional
  (base-unix
  !=
  null)
  base-unix
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null)
  cmdliner;
  propagatedBuildInputs = [
    ocaml topkg seq stdlib-shims ]
  ++
  stdenv.lib.optional
  (base-unix
  !=
  null)
  base-unix
  ++
  stdenv.lib.optional
  (cmdliner
  !=
  null)
  cmdliner;
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [
      "'ocaml'" "'pkg/pkg.ml'" "'build'" "'--dev-pkg'" "false"
      "'--with-base-unix'"
      "${if base-unix != null then "true" else "false"}" "'--with-cmdliner'" "${if
                                                                    cmdliner
                                                                    != null
                                                                    then
                                                                     
                                                                    "true"
                                                                    else
                                                                     
                                                                    "false"}" ] ];
      preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
      [ ];
      installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
      createFindlibDestdir = true;
    }
