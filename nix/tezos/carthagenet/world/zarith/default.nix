/*opam-version: "2.0"
  name: "zarith"
  version: "1.9.1"
  synopsis:
    "Implements arithmetic and logical operations over arbitrary-precision
  integers"
  description: """
  The Zarith library implements arithmetic and logical operations
  over
  arbitrary-precision integers. It uses GMP to efficiently
  implement
  arithmetic over big integers. Small integers are represented as Caml
  unboxed integers, for speed and space economy."""
  maintainer: "Xavier Leroy <xavier.leroy@inria.fr>"
  authors: ["Antoine Miné" "Xavier Leroy" "Pascal Cuoq"]
  homepage: "https://github.com/ocaml/Zarith"
  bug-reports: "https://github.com/ocaml/Zarith/issues"
  depends: [
    "ocaml"
    "ocamlfind" {build}
    "conf-gmp"
    "conf-perl" {build}
  ]
  build: [
    ["./configure"] {os != "openbsd" & os != "freebsd" & os != "macos"}
    [
      "sh"
      "-exc"
      "LDFLAGS=\"$LDFLAGS -L/usr/local/lib\" CFLAGS=\"$CFLAGS
  -I/usr/local/include\" ./configure"
    ] {os = "openbsd" | os = "freebsd"}
    [
      "sh"
      "-exc"
      "LDFLAGS=\"$LDFLAGS -L/opt/local/lib -L/usr/local/lib\"
  CFLAGS=\"$CFLAGS -I/opt/local/include -I/usr/local/include\" ./configure"
    ] {os = "macos"}
    [make]
  ]
  install: [make "install"]
  dev-repo: "git+https://github.com/ocaml/Zarith.git"
  url {
    src: "https://github.com/ocaml/Zarith/archive/release-1.9.1.tar.gz"
    checksum: [
      "md5=af41b7534a4c91a8f774f04e307c1c66"
     
  "sha512=e77620c66a59d35811acfc45c7ef3f0d50d3042194654b1f5b652a2ed5fb9d5f88e9173222e5ced286c61854434da05a4d96668089faa66ff2917afa677fc32f"
     
  "sha256=49be0214f34ae05e7a83b53351f134ba73eddaf87d1abb6f61b19943c211ca5d"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, findlib, conf-gmp, conf-perl }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "1.9.1"; in

stdenv.mkDerivation rec {
  pname = "zarith";
  version = "1.9.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/ocaml/Zarith/archive/release-1.9.1.tar.gz";
    sha256 = "0pfa271476dic5pvn6kxz3dfswxs6kqm2cxmhdx5xq2ayca05gj9";
  };
  buildInputs = [
    ocaml findlib conf-gmp conf-perl ];
  propagatedBuildInputs = [
    ocaml conf-gmp ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    (stdenv.lib.optional (!stdenv.isDarwin) "'./configure'")
    (stdenv.lib.optionals stdenv.isDarwin [
      "'sh'" "'-exc'"
      "'LDFLAGS=\"$LDFLAGS -L/opt/local/lib -L/usr/local/lib\" CFLAGS=\"$CFLAGS -I/opt/local/include -I/usr/local/include\" ./configure'"
      ])
    [ "make" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" "'install'" ] ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
