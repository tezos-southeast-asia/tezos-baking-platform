/*opam-version: "2.0"
  name: "seq"
  version: "0.1"
  synopsis:
    "Compatibility package for OCaml's standard iterator type starting from
  4.07."
  maintainer: "simon.cruanes.2007@m4x.org"
  authors: "Simon Cruanes"
  license: "GPL"
  tags: ["iterator" "seq" "pure" "list" "compatibility" "cascade"]
  homepage: "https://github.com/c-cube/seq/"
  bug-reports: "https://github.com/c-cube/seq/issues"
  depends: [
    "ocaml" {< "4.07.0"}
    "ocamlfind" {build}
    "ocamlbuild" {build}
  ]
  flags: light-uninstall
  build: [make "build"]
  install: [make "install"]
  remove: ["ocamlfind" "remove" "seq"]
  dev-repo: "git+https://github.com/c-cube/seq.git"
  url {
    src: "https://github.com/c-cube/seq/archive/0.1.tar.gz"
    checksum: "md5=0e87f9709541ed46ecb6f414bc31458c"
  }*/
{ doCheck ? false, buildAsDev ? false, stdenv, opam, fetchurl, ocaml,
  findlib, ocamlbuild }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
assert (vcompare ocaml "4.07.0") < 0;

stdenv.mkDerivation rec {
  pname = "seq";
  version = "0.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/c-cube/seq/archive/0.1.tar.gz";
    sha256 = "02lb2d9i12bxrz2ba5wygk2bycan316skqlyri0597q7j9210g8r";
  };
  buildInputs = [
    ocaml findlib ocamlbuild ];
  propagatedBuildInputs = [
    ocaml ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" "'build'" ] ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "make" "'install'" ] ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
