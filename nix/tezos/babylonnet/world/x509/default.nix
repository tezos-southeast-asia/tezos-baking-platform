/*opam-version: "2.0"
  name: "x509"
  version: "0.7.1"
  synopsis: "Public Key Infrastructure (RFC 5280, PKCS) purely in
  OCaml"
  description: """
  X.509 is a public key infrastructure used mostly on the Internet.  It
  consists
  of certificates which include public keys and identifiers, signed by
  an
  authority. Authorities must be exchanged over a second channel to establish
  the
  trust relationship. This library implements most parts of RFC5280 and
  RFC6125.
  The Public Key Cryptography Standards (PKCS) defines encoding and
  decoding
  (in ASN.1 DER and PEM format), which is also implemented by this library
  -
  namely PKCS 1, PKCS 7, PKCS 8, PKCS 9 and PKCS 10."""
  maintainer: "Hannes Mehnert <hannes@mehnert.org>"
  authors: [
    "Hannes Mehnert <hannes@mehnert.org>" "David Kaloper
  <dk505@cam.ac.uk>"
  ]
  license: "BSD-2-Clause"
  tags: "org:mirage"
  homepage: "https://github.com/mirleft/ocaml-x509"
  doc: "https://mirleft.github.io/ocaml-x509/doc"
  bug-reports: "https://github.com/mirleft/ocaml-x509/issues"
  depends: [
    "ocaml" {>= "4.04.2"}
    "dune" {>= "1.2"}
    "cstruct" {>= "4.0.0"}
    "asn1-combinators" {>= "0.2.0"}
    "ptime"
    "nocrypto" {>= "0.5.3"}
    "rresult"
    "fmt"
    "alcotest" {with-test}
    "cstruct-unix" {with-test & >= "3.0.0"}
    "gmap" {>= "0.3.0"}
    "domain-name" {>= "0.3.0"}
  ]
  build: [
    ["dune" "subst"] {pinned}
    ["dune" "build" "-p" name "-j" jobs]
    ["dune" "runtest" "-p" name "-j" jobs] {with-test}
  ]
  dev-repo: "git+https://github.com/mirleft/ocaml-x509.git"
  url {
    src:
     
  "https://github.com/mirleft/ocaml-x509/releases/download/v0.7.1/x509-v0.7.1.tbz"
    checksum: [
     
  "sha256=3cc0aecd83aee1be52caf52483608279e3d6ce5ded6e6741c49cbb50dba3d342"
     
  "sha512=10bea21d06fe67a7eb9e0eca82a603f706529eaaed85206ab0e4f469b624ac9d69045b8fed79f345b1ff3f9dc2bb1ef6acea05ff7540d08880d33ea0a1ecc009"
    ]
  }*/
{ doCheck ? false, buildDocs ? false, buildAsDev ? false, stdenv, opam,
  fetchurl, ocaml, dune, cstruct, asn1-combinators, ptime, nocrypto, rresult,
  fmt, alcotest ? null, cstruct-unix ? null, gmap, domain-name, findlib }:
let vcompare = stdenv.lib.versioning.debian.version.compare; in
let version = "0.7.1"; in
assert (vcompare ocaml "4.04.2") >= 0;
assert (vcompare dune "1.2") >= 0;
assert (vcompare cstruct "4.0.0") >= 0;
assert (vcompare asn1-combinators "0.2.0") >= 0;
assert (vcompare nocrypto "0.5.3") >= 0;
assert doCheck -> (vcompare cstruct-unix "3.0.0") >= 0;
assert (vcompare gmap "0.3.0") >= 0;
assert (vcompare domain-name "0.3.0") >= 0;

stdenv.mkDerivation rec {
  pname = "x509";
  version = "0.7.1";
  name = "${pname}-${version}";
  inherit doCheck;
  src = fetchurl
  {
    url = "https://github.com/mirleft/ocaml-x509/releases/download/v0.7.1/x509-v0.7.1.tbz";
    sha256 = "0hnklgdm1fwwqi0nfvpdbp7ddqvrh9h8697mr99bxqdfhg6sxh1w";
  };
  buildInputs = [
    ocaml dune cstruct asn1-combinators ptime nocrypto rresult fmt ]
  ++
  stdenv.lib.optional
  doCheck
  alcotest
  ++
  [
    cstruct-unix gmap domain-name findlib ];
  propagatedBuildInputs = [
    ocaml dune cstruct asn1-combinators ptime nocrypto rresult fmt ]
  ++
  stdenv.lib.optional
  doCheck
  alcotest
  ++
  [
    cstruct-unix gmap domain-name ];
  configurePhase = "true";
  postPatch = "patchShebangs .";
  buildPhase = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    [ "'dune'" "'build'" "'-p'" pname "'-j'" "1" ] (stdenv.lib.optionals
    doCheck [ "'dune'" "'runtest'" "'-p'" pname "'-j'" "1" ]) ];
  preInstall = stdenv.lib.concatMapStringsSep "\n" (stdenv.lib.concatStringsSep " ")
  [
    ];
  installPhase = "runHook preInstall; mkdir -p $out; for i in *.install; do ${opam.installer}/bin/opam-installer -i --prefix=$out --libdir=$OCAMLFIND_DESTDIR \"$i\"; done";
  createFindlibDestdir = true;
}
